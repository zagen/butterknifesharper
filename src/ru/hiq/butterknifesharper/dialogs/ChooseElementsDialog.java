package ru.hiq.butterknifesharper.dialogs;

import com.intellij.openapi.project.Project;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;
import java.util.List;

public class ChooseElementsDialog extends com.intellij.ide.util.ChooseElementsDialog<String> {

    private JCheckBox checkBox;
    private JPanel panel;

    public ChooseElementsDialog(Project project, List<? extends String> items, String title, String description, String checkboxTitle) {
        super(project, items, title, description);
        if (checkboxTitle != null) {
            checkBox = new JCheckBox(checkboxTitle,false);
            panel.add(checkBox, "South");
        }
    }


    @Override
    protected JComponent createCenterPanel() {
        this.panel = (JPanel) super.createCenterPanel();
        return panel;

    }

    @Override
    protected String getItemText(String s) {
        return s;
    }

    @Nullable
    @Override
    protected Icon getItemIcon(String s) {
        return null;
    }

    public boolean generatePrivateMethodSelected() {
        return checkBox != null && checkBox.isSelected();
    }
}
