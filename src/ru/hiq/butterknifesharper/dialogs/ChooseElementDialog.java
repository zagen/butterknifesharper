package ru.hiq.butterknifesharper.dialogs;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.DialogWrapper;
import com.intellij.ui.components.JBList;
import com.intellij.ui.components.JBScrollPane;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;
import java.util.List;


public class ChooseElementDialog extends DialogWrapper {

    @NotNull
    private final List<String> elements;
    private JBList myJBList;
    private String selected;

    public ChooseElementDialog(Project project, @NotNull List<String> elements, String title) {
        super(project);
        this.elements = elements;
        init();
        setTitle(title);
        myJBList.setSelectedValue(elements.get(0), true);
    }


    @Override
    protected JComponent createCenterPanel() {
        myJBList = new JBList(elements);
        myJBList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        myJBList.getSelectionModel().addListSelectionListener(e -> {
            Object selectedValue = myJBList.getSelectedValue();
            if (selectedValue instanceof String) {
                selected = (String) selectedValue;
            } else {
                selected = null;
            }
            setOKActionEnabled(selected != null);
        });
        myJBList.requestFocus();
        return new JBScrollPane(myJBList);
    }

    @Nullable
    public String getSelected() {
        return isOK() ? selected : null;
    }
}
