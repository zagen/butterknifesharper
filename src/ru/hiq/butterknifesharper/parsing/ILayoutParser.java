package ru.hiq.butterknifesharper.parsing;


import java.util.Map;

public interface ILayoutParser {
    Map<String, String> parse();
}
