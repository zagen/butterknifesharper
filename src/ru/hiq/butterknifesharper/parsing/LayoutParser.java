package ru.hiq.butterknifesharper.parsing;


import com.intellij.openapi.util.Pair;
import com.intellij.psi.PsiFile;
import com.intellij.psi.search.PsiShortNamesCache;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import ru.hiq.butterknifesharper.settings.IContext;
import ru.hiq.butterknifesharper.utils.StringUtils;

import javax.annotation.Nonnull;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.StringReader;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static ru.hiq.butterknifesharper.utils.FileUtils.readVirtualFile;

public class LayoutParser implements ILayoutParser {
    private List<String> ignoredTagNames = new ArrayList<>();
    private final IContext context;
    private final DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory
            .newInstance();

    public LayoutParser(IContext context) {
        this.context = context;
        this.ignoredTagNames.add("include");
        this.ignoredTagNames.add("merge");
    }

    @Override
    public Map<String, String> parse() {
        if (context.layout() == null) {
            return Collections.emptyMap();
        }
        Map<String, String> elementsToBind = new LinkedHashMap<>();

        PsiFile[] layoutFiles = PsiShortNamesCache.getInstance(context.project()).getFilesByName(String.format(Locale.getDefault(), "%s.xml", context.layout()));
        if (layoutFiles.length == 0) {
            return elementsToBind;
        }
        for (PsiFile psiFile : layoutFiles) {
            String layoutContentXml = readVirtualFile(psiFile);

            elementsToBind.putAll(findAllElementsWithIds(layoutContentXml));
        }
        return elementsToBind;
    }

    private Map<String, String> findAllElementsWithIds(String layoutContent) {
        try {
            DocumentBuilder documentBuilder = docBuilderFactory.newDocumentBuilder();
            org.w3c.dom.Document xmlDocument = documentBuilder.parse(new InputSource(new StringReader(layoutContent)));
            NodeList allElements = xmlDocument.getElementsByTagName("*");
            return parseNodeList(allElements);

        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new LinkedHashMap<>();
    }


    @Nonnull
    private Map<String, String> parseNodeList(NodeList nodeList) {
        Map<String, String> foundElements = new LinkedHashMap<>();
        for (int i = 0; i < nodeList.getLength(); i++) {
            Node node = nodeList.item(i);
            if (node.getNodeType() == Node.ELEMENT_NODE) {
                // do something with the current element
                Pair<String, String> entry = parseNode(node);
                if (entry != null) {
                    foundElements.put(entry.first, entry.second);
                }

            }
        }
        return foundElements;
    }

    private Pair<String, String> parseNode(@Nonnull Node node) {
        String className = StringUtils.trimClassName(node.getNodeName());
        className = !ignoredTagNames.contains(className) ? className : null;
        String id = null;
        Node attr = node.getAttributes().getNamedItem("android:id");
        if (attr != null) {
            Matcher idMatcher = Pattern.compile("@\\+?id\\/([_a-zA-Z0-9]*)").matcher(attr.getNodeValue());
            if (idMatcher.find()) {
                id = idMatcher.group(1);
            }
        }
        if (className != null && id != null) {
            return new Pair<>(id, className);
        }
        return null;
    }
}
