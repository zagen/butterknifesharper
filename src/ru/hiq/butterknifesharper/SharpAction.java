package ru.hiq.butterknifesharper;

import com.intellij.codeInsight.actions.ReformatCodeAction;
import com.intellij.openapi.actionSystem.ActionPlaces;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.actionSystem.DataContext;
import com.intellij.openapi.editor.Caret;
import com.intellij.openapi.editor.Editor;
import com.intellij.openapi.editor.actionSystem.EditorAction;
import com.intellij.openapi.editor.actionSystem.EditorActionHandler;
import org.jetbrains.annotations.Nullable;
import ru.hiq.butterknifesharper.choosing.ILayoutChooser;
import ru.hiq.butterknifesharper.choosing.LayoutChooser;
import ru.hiq.butterknifesharper.filtering.IElementsFilter;
import ru.hiq.butterknifesharper.filtering.UserSelectionElementFilter;
import ru.hiq.butterknifesharper.parsing.ILayoutParser;
import ru.hiq.butterknifesharper.parsing.LayoutParser;
import ru.hiq.butterknifesharper.choosing.IWriterChooser;
import ru.hiq.butterknifesharper.choosing.WriterChooser;
import ru.hiq.butterknifesharper.settings.Context;
import ru.hiq.butterknifesharper.settings.CurrentCursorPositionCalculator;
import ru.hiq.butterknifesharper.settings.IContext;
import ru.hiq.butterknifesharper.utils.*;
import ru.hiq.butterknifesharper.writing.IWriter;

import javax.annotation.Nonnull;
import java.util.*;


public class SharpAction extends EditorAction {
    public SharpAction() {
        super(new PasteBoundElementsHandler());
    }

    protected SharpAction(EditorActionHandler defaultHandler) {
        super(defaultHandler);
    }

    private static class PasteBoundElementsHandler extends EditorActionHandler {
        IStringResources resources;
        ILayoutChooser layoutChooser;
        ILayoutParser layoutParser;
        List<IElementsFilter> filters;
        IWriter writer;

        @Override
        protected void doExecute(Editor editor, @Nullable Caret caret, DataContext dataContext) {
            super.doExecute(editor, caret, dataContext);
            initDependencies(editor, caret, dataContext);

            layoutChooser.choose();

            Map<String, String> foundElements = layoutParser.parse();
            Map<String, String> elementsToBind = applyFilters(foundElements);

            if (writer.write(elementsToBind)) {
                performReformatCodeAction(dataContext);
            }

        }

        private void initDependencies(Editor editor, @Nullable Caret caret, DataContext dataContext) {
            if (editor == null)
                return;
            if (editor.getProject() == null) {
                return;
            }
            if (editor.getDocument() == null) {
                return;
            }
            this.resources = new StringResources();
            filters = new ArrayList<>();
            IContext context = new Context(dataContext, editor, editor.getDocument(), editor.getProject(), this.resources);

            IWriterChooser writerChooser = new WriterChooser(context, new CurrentCursorPositionCalculator(context));
            this.writer = writerChooser.choose();
            this.layoutChooser = new LayoutChooser(context);
            this.layoutParser = new LayoutParser(context);
            this.filters.add(new UserSelectionElementFilter(context));
        }


        @Nonnull
        private Map<String, String> applyFilters(Map<String, String> foundElements) {
            Map<String, String> result = foundElements;
            for (IElementsFilter filter : filters) {
                result = filter.filter(result);
            }
            return result;
        }

    }

    private static void performReformatCodeAction(DataContext dataContext) {
        ReformatCodeAction reformatCodeAction = new ReformatCodeAction();
        reformatCodeAction.actionPerformed(AnActionEvent.createFromAnAction(reformatCodeAction, null, ActionPlaces.UNKNOWN, dataContext));
    }

}
