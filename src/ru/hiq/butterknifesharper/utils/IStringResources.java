package ru.hiq.butterknifesharper.utils;


import javax.annotation.Nonnull;
import javax.annotation.Nullable;

public interface IStringResources {
    @Nullable
    String getString(@Nonnull String id);
}
