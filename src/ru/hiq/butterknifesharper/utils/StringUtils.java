package ru.hiq.butterknifesharper.utils;


import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringUtils {
    @Nonnull
    public final static String trimComments(@Nonnull String sourceCode) {
        return sourceCode.replaceAll("(/\\*([^*]|[\\r\\n]|(\\*+([^*/]|[\\r\\n])))*\\*+/)|(//.*)", "");
    }

    @Nullable
    public final static String trimClassName(@Nonnull String className) {
        Pattern pattern = Pattern.compile("^([0-9a-zA-Z_]*\\.)*([0-9a-zA-Z_]*)$");
        Matcher matcher = pattern.matcher(className);
        if (matcher.find()) {
            return matcher.group(2);
        }
        return null;
    }
}
