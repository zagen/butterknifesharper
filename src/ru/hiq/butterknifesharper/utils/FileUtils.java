package ru.hiq.butterknifesharper.utils;


import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.PsiFile;

import javax.annotation.Nullable;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

public class FileUtils {

    @Nullable
    public static String readVirtualFile(PsiFile psiFile) {
        VirtualFile file = psiFile.getVirtualFile();

        String layoutContent = null;
        try {
            layoutContent = new String(file.contentsToByteArray(), StandardCharsets.UTF_8);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return layoutContent;
    }
}
