package ru.hiq.butterknifesharper.utils;


import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.HashMap;
import java.util.Map;

public class StringResources implements IStringResources {
    public final static String ERROR_NO_OPEN_DOCUMENTS = "no open documents";
    public final static String ERROR_NO_LAYOUT_FOUND_TITLE = "no layout found title";
    public final static String ERROR_NO_LAYOUT_FOUND_DESCRIPTION = "no layout found description";
    public final static String MESSAGE_INPUT_LAYOUT_NAME = "input layout name";
    public static final String MESSAGE_SELECT_BOUND_ELEMENT_TITLE = "select bound element";
    public static final String MESSAGE_SELECT_BOUND_ELEMENT_DESCRIPTION = "select bound element desc";
    public static final String MESSAGE_GENERATE_PRIVATE_METHOD = "generate private method";
    public static final String MESSAGE_SELECT_LAYOUT_DIALOG_TITLE = "select layout title";


    private final Map<String, String> localisations = new HashMap<>();

    public StringResources() {
        localisations.put(ERROR_NO_OPEN_DOCUMENTS, "Can't read current document!");
        localisations.put(ERROR_NO_LAYOUT_FOUND_TITLE, "No layout found!");
        localisations.put(ERROR_NO_LAYOUT_FOUND_DESCRIPTION, "Do you want to input layout name manually?");
        localisations.put(MESSAGE_INPUT_LAYOUT_NAME, "Input layout name");
        localisations.put(MESSAGE_SELECT_BOUND_ELEMENT_TITLE, "Select elements");
        localisations.put(MESSAGE_SELECT_BOUND_ELEMENT_DESCRIPTION, "Choosen elements will be bound in code");
        localisations.put(MESSAGE_GENERATE_PRIVATE_METHOD, "Generate method for class members initialisation even if it exist");
        localisations.put(MESSAGE_SELECT_LAYOUT_DIALOG_TITLE, "Select layout to bind from");
    }


    @Override
    @Nullable
    public String getString(@Nonnull String id) {
        return localisations.get(id);
    }
}
