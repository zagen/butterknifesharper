package ru.hiq.butterknifesharper.filtering;


import javax.annotation.Nonnull;
import java.util.Map;

public interface IElementsFilter {
    @Nonnull
    Map<String, String> filter(@Nonnull Map<String, String> source);
}
