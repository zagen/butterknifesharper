package ru.hiq.butterknifesharper.filtering;


import ru.hiq.butterknifesharper.settings.IContext;

import javax.annotation.Nonnull;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AlreadyExistElementFilter implements IElementsFilter {
    private final String text;

    public AlreadyExistElementFilter(IContext context) {
        this.text = context.document().getText();
    }

    @Nonnull
    @Override
    public Map<String, String> filter(@Nonnull Map<String, String> source) {
        Map<String, String> result = new LinkedHashMap<>();
        for (String id : source.keySet()) {
            if (!isInCodeAlready(id, text)) {
                result.put(id, source.get(id));
            }
        }
        return result;
    }

    private boolean isInCodeAlready(@Nonnull String id, @Nonnull String source) {
        return isFindById(id, source) || isFindWithButterKnife(id, source);
    }

    private boolean isFindWithButterKnife(String id, String source) {
        return findPattern("@BindView[\\s]*\\([\\s]*R\\.id\\.%s[\\s]*\\)", id, source);
    }


    private boolean isFindById(@Nonnull String id, @Nonnull String source) {
        return findPattern("findViewById[\\s]*\\([\\s]*R\\.id\\.%s[\\s]*\\)", id, source);
    }

    private boolean findPattern(@Nonnull String p, @Nonnull String id, @Nonnull String source) {
        Pattern pattern = Pattern.compile(String.format(Locale.getDefault(), p, id));
        Matcher matcher = pattern.matcher(source);
        return matcher.find();
    }

}
