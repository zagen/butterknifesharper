package ru.hiq.butterknifesharper.filtering;

import ru.hiq.butterknifesharper.settings.IContext;
import ru.hiq.butterknifesharper.dialogs.ChooseElementsDialog;
import ru.hiq.butterknifesharper.utils.StringResources;

import javax.annotation.Nonnull;
import java.util.*;

public class UserSelectionElementFilter implements IElementsFilter {

    private final IContext context;

    public UserSelectionElementFilter(IContext context) {
        this.context = context;
    }

    @Nonnull
    @Override
    public Map<String, String> filter(@Nonnull Map<String, String> source) {
        if (source.size() == 0) {
            return source;
        }
        Map<String, String> result = new LinkedHashMap<>();
        Collection<String> ids = source.keySet();
        ChooseElementsDialog dialog = new ChooseElementsDialog(context.project(),
                new ArrayList<>(ids),
                context.resources().getString(StringResources.MESSAGE_SELECT_BOUND_ELEMENT_TITLE),
                context.resources().getString(StringResources.MESSAGE_SELECT_BOUND_ELEMENT_DESCRIPTION),
                !context.butterknifeExist() ? context.resources().getString(StringResources.MESSAGE_GENERATE_PRIVATE_METHOD) : null
        );
        List<String> selectedIds = dialog.showAndGetResult();
        context.setGeneratePrivateMethod(dialog.generatePrivateMethodSelected());
        for (String id : selectedIds) {
            result.put(id, source.get(id));
        }
        return result;
    }
}
