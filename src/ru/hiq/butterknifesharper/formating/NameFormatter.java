package ru.hiq.butterknifesharper.formating;


import javax.annotation.Nonnull;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class NameFormatter implements IFormatter<String> {
    @Nonnull
    @Override
    public String format(@Nonnull String source) {
        Pattern pattern = Pattern.compile("(/*)_([a-zA-Z0-9]{1})(/*)");
        Matcher matcher = pattern.matcher(source);
        StringBuffer sb = new StringBuffer();
        while (matcher.find()) {
            String rep = matcher.group(1) + matcher.group(2).toUpperCase() + matcher.group(3);
            matcher.appendReplacement(sb, rep);
        }
        matcher.appendTail(sb);
        return sb.toString();
    }
}
