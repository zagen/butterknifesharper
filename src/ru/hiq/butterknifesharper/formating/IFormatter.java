package ru.hiq.butterknifesharper.formating;


import javax.annotation.Nonnull;

public interface IFormatter<T> {
    @Nonnull
    String format(@Nonnull T source);
}
