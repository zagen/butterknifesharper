package ru.hiq.butterknifesharper.choosing;


import com.intellij.openapi.editor.Editor;
import com.intellij.openapi.project.Project;
import com.intellij.psi.PsiFile;
import com.intellij.psi.search.PsiShortNamesCache;
import ru.hiq.butterknifesharper.settings.IContext;
import ru.hiq.butterknifesharper.settings.IInsertPositionCalculator;
import ru.hiq.butterknifesharper.utils.FileUtils;
import ru.hiq.butterknifesharper.utils.StringUtils;
import ru.hiq.butterknifesharper.writing.ButterknifeStyleWriter;
import ru.hiq.butterknifesharper.writing.ClassicStyleWriter;
import ru.hiq.butterknifesharper.writing.IWriter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class WriterChooser implements IWriterChooser {
    private final IContext context;
    private final IInsertPositionCalculator calculator;

    public WriterChooser(IContext context, IInsertPositionCalculator calculator) {
        this.context = context;
        this.calculator = calculator;
    }


    @Override
    public IWriter choose() {
        context.setButterknifeExist(gradleHasButterknifeDependency(context.editor()));
        return context.butterknifeExist() ? new ButterknifeStyleWriter(context, calculator) : new ClassicStyleWriter(context, calculator);
    }

    private boolean gradleHasButterknifeDependency(Editor editor) {
        List<PsiFile> files = getFilesByNames(editor.getProject(), getGradleFileNames(editor.getProject()));
        for (PsiFile file : files) {

            boolean hasButterknife = isGradleFileHasButterknifeAsDependency(file);
            if (hasButterknife) {
                return true;
            }
        }
        return false;
    }

    private boolean isGradleFileHasButterknifeAsDependency(PsiFile file) {
        Pattern pattern = Pattern.compile("compile[\\s]*(\\'|\\\")com.jakewharton:butterknife:[0-9\\.]+(\\'|\")");
        Matcher matcher;
        String gradleFileContent = FileUtils.readVirtualFile(file);
        if (gradleFileContent != null) {
            matcher = pattern.matcher(StringUtils.trimComments(gradleFileContent));
            if (matcher.find())
                return true;
        }

        return false;
    }


    private List<String> getGradleFileNames(Project project) {
        String[] allFiles = PsiShortNamesCache.getInstance(project).getAllFileNames();
        List<String> filesWithGradleExtension = new ArrayList<>();
        for (String fileName : allFiles) {
            if (fileName.endsWith(".gradle")) {
                filesWithGradleExtension.add(fileName);
            }
        }
        return filesWithGradleExtension;
    }

    private List<PsiFile> getFilesByNames(Project project, List<String> filenames) {
        PsiShortNamesCache cache = PsiShortNamesCache.getInstance(project);
        List<PsiFile> files = new ArrayList<>();
        for (String filename : filenames) {
            PsiFile[] psiFiles = cache.getFilesByName(filename);
            files.addAll(Arrays.asList(psiFiles));
        }
        return files;
    }
}
