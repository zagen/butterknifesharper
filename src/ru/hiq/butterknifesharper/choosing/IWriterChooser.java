package ru.hiq.butterknifesharper.choosing;

import ru.hiq.butterknifesharper.writing.IWriter;

public interface IWriterChooser {
    IWriter choose();
}
