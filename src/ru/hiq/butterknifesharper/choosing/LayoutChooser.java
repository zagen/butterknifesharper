package ru.hiq.butterknifesharper.choosing;


import com.intellij.openapi.editor.Document;
import com.intellij.openapi.ui.MessageType;
import com.intellij.openapi.ui.Messages;
import com.intellij.openapi.vcs.VcsShowConfirmationOption;
import com.intellij.util.ui.ConfirmationDialog;
import ru.hiq.butterknifesharper.settings.IContext;
import ru.hiq.butterknifesharper.dialogs.ChooseElementDialog;
import ru.hiq.butterknifesharper.utils.NotificationUtils;
import ru.hiq.butterknifesharper.utils.StringResources;
import ru.hiq.butterknifesharper.utils.StringUtils;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LayoutChooser implements ILayoutChooser {
    private final IContext context;

    public LayoutChooser(IContext context) {
        this.context = context;
    }

    @Override
    public void choose() {
        Document document = context.document();
        if (document == null) {
            NotificationUtils.notificate(context.project(), context.resources().getString(StringResources.ERROR_NO_OPEN_DOCUMENTS), MessageType.INFO);
            context.setLayout(null);
            return;
        }
        Map<String, Boolean> layouts = findLayoutNamesInSelectedDocument(document);
        if (layouts.size() == 0) {
            ConfirmationDialog confirmationDialog = new ConfirmationDialog(context.project(),
                    context.resources().getString(StringResources.ERROR_NO_LAYOUT_FOUND_DESCRIPTION),
                    context.resources().getString(StringResources.ERROR_NO_LAYOUT_FOUND_TITLE),
                    null, VcsShowConfirmationOption.STATIC_SHOW_CONFIRMATION);
            confirmationDialog.show();
            if (confirmationDialog.isOK()) {
                Messages.InputDialog inputDialog = new Messages.InputDialog(context.project(),
                        context.resources().getString(StringResources.MESSAGE_INPUT_LAYOUT_NAME),
                        null,
                        null,
                        null,
                        null);

                inputDialog.showAndGet();
                String inputedLayoutName = inputDialog.getInputString().trim();
                context.setLayout(!inputedLayoutName.isEmpty() ? inputedLayoutName : null);
                context.setPrivateMethodHasParam(false);
            }
        } else if (layouts.size() > 1) {
            ChooseElementDialog dialog = new ChooseElementDialog(context.project(), new ArrayList<>(layouts.keySet()), context.resources().getString(StringResources.MESSAGE_SELECT_LAYOUT_DIALOG_TITLE));

            dialog.show();
            String selected = dialog.getSelected();

            context.setLayout(selected);
            context.setPrivateMethodHasParam(selected != null ? layouts.getOrDefault(dialog.getSelected(), false) : false);
        } else {
            String layout = new ArrayList<>(layouts.keySet()).get(0);
            context.setLayout(layout);
            context.setPrivateMethodHasParam(layouts.get(layout));
        }
    }

    @Nonnull
    private Map<String, Boolean> findLayoutNamesInSelectedDocument(@Nonnull Document document) {
        Map<String, Boolean> names = new HashMap<>();
        String sourceCode = document.getText();
        String pattern = ";[^;=]*(=?)[^;=]*R\\.layout\\.([a-zA-Z_0-9]+)";
        Pattern regex = Pattern.compile(pattern);
        Matcher matcher = regex.matcher(StringUtils.trimComments(sourceCode));
        while (matcher.find()) {
            names.put(matcher.group(2), !matcher.group(1).trim().isEmpty());
        }
        return names;
    }
}
