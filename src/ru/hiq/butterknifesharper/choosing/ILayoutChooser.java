package ru.hiq.butterknifesharper.choosing;


public interface ILayoutChooser {
    void choose();
}
