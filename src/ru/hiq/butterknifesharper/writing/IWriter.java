package ru.hiq.butterknifesharper.writing;


import javax.annotation.Nonnull;
import java.util.Map;

public interface IWriter {

    boolean write(@Nonnull Map<String, String> elementsToBind);
}
