package ru.hiq.butterknifesharper.writing;


import com.intellij.openapi.application.ApplicationManager;
import ru.hiq.butterknifesharper.settings.IContext;
import ru.hiq.butterknifesharper.settings.IInsertPositionCalculator;
import ru.hiq.butterknifesharper.formating.IFormatter;
import ru.hiq.butterknifesharper.formating.NameFormatter;

import javax.annotation.Nonnull;
import java.util.Locale;
import java.util.Map;

public class ButterknifeStyleWriter implements IWriter {
    private IInsertPositionCalculator positionCalculator;
    private IContext context;
    private IFormatter<String> nameFormatter = new NameFormatter();

    public ButterknifeStyleWriter(IContext context, IInsertPositionCalculator positionCalculator) {
        this.positionCalculator = positionCalculator;
        this.context = context;
    }


    @Override
    public boolean write(@Nonnull Map<String, String> elements) {
        if (context.layout() == null) {
            return false;
        }
        StringBuilder builder = new StringBuilder();
        if (elements.size() != 0) {
            for (String id : elements.keySet()) {
                builder.append(String.format(Locale.getDefault(), "@BindView(R.id.%s)\n", id));
                builder.append(String.format(Locale.getDefault(), "%s %s;\n", elements.get(id), nameFormatter.format(id)));
                builder.append("\n");
            }
            ApplicationManager.getApplication().runWriteAction(() -> {
                context.document().insertString(positionCalculator.calculatePosition(), builder.toString());
            });
            return true;
        }
        return false;
    }
}
