package ru.hiq.butterknifesharper.writing;

import com.intellij.openapi.application.ApplicationManager;
import ru.hiq.butterknifesharper.settings.IContext;
import ru.hiq.butterknifesharper.settings.IInsertPositionCalculator;
import ru.hiq.butterknifesharper.utils.StringUtils;
import ru.hiq.butterknifesharper.formating.IFormatter;
import ru.hiq.butterknifesharper.formating.NameFormatter;

import javax.annotation.Nonnull;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ClassicStyleWriter implements IWriter {
    private final static String INIT_METHOD_NAME = "initViews";
    private final static String PARENT_NAME = "view";

    private IInsertPositionCalculator positionCalculator;
    private IContext context;

    private IFormatter<String> nameFormatter = new NameFormatter();

    public ClassicStyleWriter(IContext context, IInsertPositionCalculator positionCalculator) {
        this.positionCalculator = positionCalculator;
        this.context = context;
    }


    @Override
    public boolean write(@Nonnull Map<String, String> elementsToBind) {
        StringBuilder members = new StringBuilder();
        StringBuilder membersInitialisations = new StringBuilder();

        if (elementsToBind.size() != 0) {
            membersInitialisations.append("\n");
            for (String id : elementsToBind.keySet()) {
                String className = elementsToBind.get(id);
                String memberName = nameFormatter.format(id);
                String parentView = context.hasPrivateMethodWithParam() ? PARENT_NAME + "." : "";
                members.append(String.format(Locale.getDefault(), "private %s %s;\n", className, memberName));
                membersInitialisations.append(String.format(Locale.getDefault(),
                        "\t%s = (%s)%sfindViewById(R.id.%s);", memberName, className, parentView, id));
            }
            int membersInitPosition = positionInInitViewMethodBegining(context);
            boolean methodExist = membersInitPosition != -1;
            if (methodExist && !context.shouldGeneratePrivateMethod()) {
                ApplicationManager.getApplication().runWriteAction(() -> {
                    context.document().insertString(positionCalculator.calculatePosition(), members.toString());
                    context.document().insertString(positionInInitViewMethodBegining(context), membersInitialisations.toString());
                });

            } else {
                String method = generatePrivateMethod(context, membersInitialisations);
                ApplicationManager.getApplication().runWriteAction(() -> {
                    context.document().insertString(positionCalculator.calculatePosition(), String.format(Locale.getDefault(), "%s\n%s\n",
                            members.toString(),
                            method));
                });
            }
            return true;
        }

        return false;
    }

    private String generatePrivateMethod(IContext context, StringBuilder membersInitialisations) {
        return String.format(Locale.getDefault(), "private void %s(%s){%s\n}",
                INIT_METHOD_NAME,
                context.hasPrivateMethodWithParam() ? "View " + PARENT_NAME : "",
                membersInitialisations.toString());
    }

    private int positionInInitViewMethodBegining(IContext context) {
        String text = StringUtils.trimComments(context.document().getText());
        boolean hasParam = context.hasPrivateMethodWithParam();
        Pattern regex = Pattern.compile(String.format(Locale.getDefault(),
                "%s[\\s]*\\(([\\s]*%s)\\)[\\s]*\\{", INIT_METHOD_NAME, hasParam ? "[a-zA-Z0-9\\s]+" : ""));
        Matcher matcher = regex.matcher(text);
        if (matcher.find()) {
            String initMethodSignature = matcher.group(0);
            return text.indexOf(initMethodSignature) + initMethodSignature.length();
        }
        return -1;
    }

}

