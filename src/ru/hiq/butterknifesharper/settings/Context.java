package ru.hiq.butterknifesharper.settings;

import com.intellij.openapi.actionSystem.DataContext;
import com.intellij.openapi.editor.Document;
import com.intellij.openapi.editor.Editor;
import com.intellij.openapi.project.Project;
import ru.hiq.butterknifesharper.utils.IStringResources;


public class Context implements IContext {
    private final DataContext dataContext;
    private final Editor editor;
    private final Document document;
    private final Project project;
    private final IStringResources resources;
    private String layout;
    private boolean isButterknifeExist;
    private boolean shouldGeneratePrivateMethod;
    private boolean hasMethodParam;

    public Context(DataContext dataContext, Editor editor, Document document, Project project, IStringResources resources) {
        this.dataContext = dataContext;
        this.editor = editor;
        this.document = document;
        this.project = project;
        this.resources = resources;
    }

    @Override
    public Project project() {
        return project;
    }

    @Override
    public IStringResources resources() {
        return resources;
    }

    @Override
    public Editor editor() {
        return editor;
    }

    @Override
    public Document document() {
        return document;
    }

    @Override
    public DataContext dataContext() {
        return dataContext;
    }

    @Override
    public String layout() {
        return layout;
    }

    @Override
    public void setLayout(String layout) {
        this.layout = layout;
    }

    @Override
    public boolean butterknifeExist() {
        return isButterknifeExist;
    }

    @Override
    public void setButterknifeExist(boolean isExist) {
        this.isButterknifeExist = isExist;
    }

    @Override
    public void setGeneratePrivateMethod(boolean generate) {
        this.shouldGeneratePrivateMethod = generate;
    }

    @Override
    public boolean shouldGeneratePrivateMethod() {
        return shouldGeneratePrivateMethod;
    }

    @Override
    public boolean hasPrivateMethodWithParam() {
        return hasMethodParam;
    }

    @Override
    public void setPrivateMethodHasParam(boolean hasParam) {
        this.hasMethodParam = hasParam;
    }
}
