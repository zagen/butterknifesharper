package ru.hiq.butterknifesharper.settings;

public interface IInsertPositionCalculator {

    int calculatePosition();
}
