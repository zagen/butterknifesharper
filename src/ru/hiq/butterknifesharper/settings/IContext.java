package ru.hiq.butterknifesharper.settings;


import com.intellij.openapi.actionSystem.DataContext;
import com.intellij.openapi.editor.Document;
import com.intellij.openapi.editor.Editor;
import com.intellij.openapi.project.Project;
import ru.hiq.butterknifesharper.utils.IStringResources;

public interface IContext {
    Project project();

    IStringResources resources();

    Editor editor();

    Document document();

    DataContext dataContext();

    String layout();

    void setLayout(String layout);

    boolean butterknifeExist();

    void setButterknifeExist(boolean isExist);

    void setGeneratePrivateMethod(boolean generate);

    boolean shouldGeneratePrivateMethod();

    boolean hasPrivateMethodWithParam();

    void setPrivateMethodHasParam(boolean hasParam);
}
