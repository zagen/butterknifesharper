package ru.hiq.butterknifesharper.settings;


public class CurrentCursorPositionCalculator implements IInsertPositionCalculator {
    private IContext context;

    public CurrentCursorPositionCalculator(IContext context) {
        this.context = context;
    }

    @Override
    public int calculatePosition() {
        return context != null ? context.editor().getCaretModel().getOffset() : 0;
    }
}
